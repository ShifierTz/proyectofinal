import { app } from './conn.js';
import { getAuth, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/10.6.0/firebase-auth.js";

const auth = getAuth(app);

document.getElementById('btnEnviar').addEventListener('click', async (e) => {
        e.preventDefault();
        const email = document.getElementById('correo').value;
        const password = document.getElementById('contraseña').value;
        await signInWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
        const user = userCredential.user;
        window.location.href = '/html/menuAdmin.html';
        })
        .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        alert("Correo o contraseña incorrectos");
        })
    }
);  



