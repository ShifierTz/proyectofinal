import {app} from "./conn.js";
import {getDatabase,ref, set, onValue, get, child, remove} from "https://www.gstatic.com/firebasejs/10.6.0/firebase-database.js";
import {getDownloadURL} from "https://www.gstatic.com/firebasejs/10.6.0/firebase-storage.js";
import { upload } from "./storage.js";

const db = getDatabase(app);

function obtenerId() {
    const refdb = ref(db);
  
    return get(child(refdb, 'cant'))
      .then((snapshot) => {
        if (snapshot.exists()) {
          console.log(snapshot.val().prod + 1);
          return snapshot.val().prod + 1;
        } else {
          console.log('No hay datos disponibles');
          return null; // Otra opción es lanzar un error aquí si es crítico
        }
      })
      .catch((error) => {
        console.error('Error al obtener datos:', error);
        throw error; // Lanza el error para que sea manejado externamente si es necesario
      });
  }

export function displayT(){
    const dbRef=ref(db,'productos');
    onValue(dbRef,(snaphot) => {
      snaphot.forEach((childSnapshot) => {
        const data = childSnapshot.val(); 
  
        const div = document.createElement('div');
        div.className = 'card';
        div.style.width = '18rem';
        div.style.marginTop = '20px';
  
        const img = document.createElement('img');
        img.src = data.img; // Reemplaza 'producto.imagenURL' con la URL de la imagen del producto
        img.className = 'card-img-top';
        img.alt = data.nombre; // Reemplaza 'producto.nombre' con el nombre del producto
        img.style.height = '250px';
  
        const cardBody = document.createElement('div');
        cardBody.className = 'card-body';
  
        const cardTitle = document.createElement('h5');
        cardTitle.className = 'card-title';
        cardTitle.textContent = data.nombre; // Reemplaza 'producto.nombre' con el nombre del producto
  
        const cardText = document.createElement('p');
        cardText.className = 'card-text';
        cardText.textContent = data.desc;
        const cardLink = document.createElement('a');
        cardLink.className = 'btn btn-warning';
        cardLink.textContent = 'Comprar';
        cardLink.href = '#';
  
        cardBody.appendChild(cardTitle);
        cardBody.appendChild(cardText);
        cardBody.appendChild(cardLink);
  
        div.appendChild(img);
        div.appendChild(cardBody);
  
        document.getElementById("grid").appendChild(div);
      });
    },{
      onlyOnce: true
    });
  }

export function displayA(){
    var divCount = 0;
    const dbRef=ref(db,'productos');
    onValue(dbRef,(snaphot) => {
      snaphot.forEach((childSnapshot) => {
        const key = childSnapshot.key;
        const data = childSnapshot.val(); 
  
        const div = document.createElement('div');
        div.className = 'card';
        div.style.width = '18rem';
        div.style.marginTop = '20px';
  
        const img = document.createElement('img');
        img.src = data.img; 
        img.className = 'card-img-top';
        img.alt = data.nombre; 
        img.style.height = '250px';
  
        const cardBody = document.createElement('div');
        cardBody.className = 'card-body';
  
        const cardTitle = document.createElement('h5');
        cardTitle.className = 'card-title';
        cardTitle.textContent = data.nombre; 
  
        const cardText = document.createElement('p');
        cardText.className = 'card-text';
        cardText.textContent = data.desc;
        
        const btnM = document.createElement('button');
        btnM.className = 'btn btn-outline-secondary';
        btnM.textContent = 'Modificar';
        btnM.style = "margin-right: 10px;";
        btnM.onclick = () => ventanaModificar(key);

        const btnE = document.createElement('button');
        btnE.className = 'btn btn-danger';
        btnE.textContent = 'Eliminar';
        btnE.onclick = () => eliminar(key);

        cardBody.appendChild(cardTitle);
        cardBody.appendChild(cardText);
        cardBody.appendChild(btnM);
        cardBody.appendChild(btnE);

        div.appendChild(img);
        div.appendChild(cardBody);

        document.getElementById("grid").appendChild(div);

        divCount++;
      });
      console.log("Cantidad de div generados: "+divCount);
    },{
      onlyOnce: true
    }); 
}

function ventanaModificar(llave){
    const miModal = new bootstrap.Modal(document.getElementById('mod'));
    miModal.show();
    const dbRef = ref(db);
    var nombre = document.getElementById("nombrem");
    var desc = document.getElementById("descm");
    var link;
    get(child(dbRef, `productos/${llave}`)).then((snapshot) => {
        if (snapshot.exists()) {
          nombre.value = snapshot.val().nombre;
          desc.value = snapshot.val().desc;
          link = snapshot.val().img;
        } else {
          console.log("No data available");
        }
      }).catch((error) => {
        console.error(error);
      }
    );

    document.getElementById("modificar").addEventListener("click", function(){
        if(document.getElementById("imgmod").files.length === 0){
          set(ref(db, `productos/${llave}`), {
            nombre: nombre.value,
            desc: desc.value,
            img: link
          });
          alert("Producto modificado con éxito");
          location.reload(true);
        } else {
          const fileItem = document.getElementById("imgmod").files[0];
          const fileName = fileItem.name;
          var imagen = upload(fileItem, fileName);
    
          setTimeout(() => {
            getDownloadURL(imagen)
              .then((result) => {
                const url = result;
                console.log("URL de descarga del archivo:", url);
                set(ref(db, `productos/${llave}`), {
                  nombre: nombre.value,
                  desc: desc.value,
                  img: url
                });
                alert("Producto modificado con éxito");
                location.reload(true);
              })
              .catch((error) => {
                console.error("Error al obtener la URL de descarga:", error);
              });
          }, 5000);
        }
        
      })

}

export function añadir(img,nombre,desc){
    var imagen = upload(img.files[0], img.files[0].name);
    obtenerId()
    .then((id) => {
      if (id !== null) {
        setTimeout(() => {
            getDownloadURL(imagen)
              .then((result) => {
                console.log("URL de descarga del archivo:", result);
                set(ref(db, 'productos/' + "art" + id), {
                  nombre: nombre,
                  desc: desc,
                  img: result
                });
                set(ref(db,'cant'),{prod: id});
                alert("Producto añadido con éxito");
                location.reload(true);
              })
              .catch((error) => {
                console.error("Error al obtener la URL de descarga:", error);
              });
          }, 5000);
        console.log('ID obtenido:', id);
      } else {
        console.log('No se pudo obtener el ID.');
      }
    })
    .catch((error) => {
      console.error('Error general:', error);
    });;
  }

function eliminar(llave){
remove(ref(db, `productos/${llave}`));
  alert("Elemento removido con éxito");
  location.reload(true);
}
