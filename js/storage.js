import { app } from "./conn.js";
import {getStorage, ref, uploadBytes} from "https://www.gstatic.com/firebasejs/10.6.0/firebase-storage.js";

const gSt = getStorage(app);
export function upload(file,fileName){
    const childRef = ref(gSt,'img/'+fileName)
    uploadBytes(childRef,file).then((snapshot) => console.log('uploaded'))
    return childRef; 
}