import { initializeApp } from "https://www.gstatic.com/firebasejs/10.6.0/firebase-app.js";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyBiaGDbQg4XkFUS9yWHomueWEoeXGBsDu0",
    authDomain: "proyectofinal-c24b6.firebaseapp.com",
    projectId: "proyectofinal-c24b6",
    storageBucket: "proyectofinal-c24b6.appspot.com",
    messagingSenderId: "173047085195",
    appId: "1:173047085195:web:265be854291c5f57cc3c8e"
  };

  // Initialize Firebase
  export const app = initializeApp(firebaseConfig);